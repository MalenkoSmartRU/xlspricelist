<?php

namespace App\Http\Controllers;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Illuminate\Http\Request;

class XLSReading extends Controller
{
    protected $fileName;        // Имя файла на локальной машине
    protected $fileLink;        // Ссылка на файл на удаленном сервере
    protected $numRow;          // Номер строки, с которой должно начаться считывание xls Файла
    
    public function __construct() {
        $this->fileName = config('xls.file');
        $this->fileLink = config('xls.link');
        $this->numRow   = config('xls.column');
    }
    
    public function show()
    {
        $values = $this->divideSheet($this->activeSheet());
        $this->insertData($values['categories'], $values['products']);
        return view('xlspricelist', ['categories' => $this->getFrontCategories()]);
    }
    
    // Загрузка файла и получение активной страницы
    protected function activeSheet()
    {
        // Если файла не существует, загрузить его снова
        if (!Storage::disk('local')->exists($this->fileName)) {
            // Сохранить удаленный файл на локальном сервере
            Storage::disk('local')->put($this->fileName, file_get_contents($this->fileLink));
        }
        
        // Создать новый объект Spreadsheet
        $localFileName = Storage::disk('local')->path($this->fileName);
        $spreadsheet = IOFactory::load($localFileName);
        
        // Получить массив ячеек из файла
        $activeSheet = $spreadsheet->getActiveSheet();
        
        return $activeSheet;
    }
    
    // Получение массивов категорий и продуктов
    protected function divideSheet(Worksheet $activeSheet)
    {
        $sheetData = $activeSheet->toArray(null, true, true, true);
        
        // Массивы категорий, товаров и идентификаторов последних категорий
        $categories = $products = $lastLevelIds = [];

        // Идентификаторы каталога и товаров
        $idCat = $idProd = 1;
        
        //
        for ($i = $this->numRow; $i <= $activeSheet->getHighestRow() - 1; $i++) {
            
            $currentLevel = $activeSheet->getRowDimension($i)->getOutlineLevel();
            
            // Это категория, если цена не установлена
            if ($sheetData[$i]['C'] === null) {
                
                $lastLevelIds[$currentLevel] = $idCat;
                
                // Заполнение массива категорий
                $categories[$idCat] = [
                    'id_category' => $idCat,
                    'id_parent' => ($currentLevel === 1)? 0 : $lastLevelIds[$currentLevel - 1],
                    'name' => $sheetData[$i]['B']
                ];
                
                $idCat++;
            } else {

                // Заполнение массива товаров
                $products[$idProd] = [
                    'id_product' => $idProd,
                    'id_category' => $idCat - 1,
                    'name' => $sheetData[$i]['B'],
                    'price' => (float)trim(str_replace(['\'', 'руб.'], '', $sheetData[$i]['C'])),
                    'measure' => $sheetData[$i]['D']
                ];
                $idProd++;
            }
        }
        
        return ['categories' => $categories, 'products' => $products];
    }
    
    // Заполнение таблиц категорий и продуктов
    protected function insertData($categories, $products)
    {
        // Предварительно очистить старые значения в таблицах
        DB::table('products')->delete();
        DB::table('categories')->delete();
        
        // Внести новые значения
        DB::table('categories')->insert($categories);
        DB::table('products')->insert($products);
    }
    
    // Получение заголовков категорий, которые будут отображены
    protected function getFrontCategories()
    {
        return  DB::table('categories')
                ->select()
                ->where('id_parent', '=', 0)
                ->get()
                ->all();
    }
    
    // Вывод запрашиваемой секции
    public function section(Request $request)
    {
        // Получить идентификатор переданной категории
        $id = $request->input('root');
        
        $currentCategory = DB::table('categories')->select(['name'])->where('id_category', '=', $id)->get()->first();
        $categories = DB::table('categories')->select(['name', 'id_category AS id'])->where('id_parent', '=', $id)->get()->all();
        $products = DB::table('products')->select(['id_product AS id', 'name', 'price', 'measure'])->where('id_category', '=', $id)->get()->all();
        
        $jsonCat = $jsonProd = [];

        foreach ($categories as $category) {
            $jsonCat[] = [
                'isCategory' => true,
                'id' => $category->id,
                'text' => $category->name,
                'classes' => 'folder',
                'hasChildren' => true
            ];
        }
        
        foreach ($products as $product) {
            $jsonProd[] = [
                'isCategory' => false,
                'id' => $product->id,
                'text' => $product->name,
                'price' => $product->price,
                'measure' => $product->measure,
                'classes' => 'file',
                'hasChildren' => false
            ];
        }
        
        return json_encode(['category' => $currentCategory, 'section' => array_merge($jsonProd, $jsonCat)], JSON_UNESCAPED_UNICODE);
    }
}