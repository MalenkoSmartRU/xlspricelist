<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Вывод содержимого xls файла</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet"> 
        
        <!-- Styles -->
        <link rel="stylesheet" href="/css/jquery.treeview.css" />
        <link rel="stylesheet" href="/css/screen.css" />
        <link rel="stylesheet" href="/css/style.css" />
        
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Noto Sans', sans-serif;
                height: 100vh;
                margin: 0;
            }
        </style>
        
        <!-- Scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
        
        <script>
            $(document).ready(function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            });
        </script>
        
        <script src="/js/products.js"></script>
        <script src="/js/jquery.cookie.js"></script>
        <script src="/js/jquery.treeview.js"></script>
        <script src="/js/jquery.treeview.edit.js"></script>
        <script src="/js/jquery.treeview.async.js"></script>
        <script src="/js/treeview.js"></script>

    </head>
    <body>
        <div id="pricelist">
            <div id="tree">
                <ul id="list" class="filetree">
                    @foreach($categories as $category)
                    <li id="{{ $category->id_category }}" class="hasChildren">
                        <span class="folder">{{ $category->name }}</span>
                        <ul>
                            <li><span class="placeholder"></span></li>
                        </ul>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div id="products">
                <h1 id="products-category"></h1>
                <div id="products-content"></div>
            </div>
        </div>
    </body>
</html>
