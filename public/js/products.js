/*
 * Выполняет обработку полученного списка продуктов
 */
function loadCategory(response)
{
    // Установить заголовок категории
    $('#products-category').html(response.category.name);
    // Получить список товаров в категории
    $('#products-content').html('<table id="products-list"><tbody></tbody></table>');
    
    count = 0; // Количество товаров в категории
    // Перебрать все товары и вывести их на экран
    response.section.forEach(function(section) {
        if (!section.isCategory) {
            count++;
            $('#products-list').append('<tr><td>'+section.id+'</td><td>'+section.text+'</td><td>'+section.price+' руб/'+section.measure+'</tr>');
        }
    });
    
    if (!count) {
        $('#products-content').html('Нет товаров в выбранной категории, ищите в подкатегориях');
    } else {
        $('#products-list').prepend('<thead><tr><th>Артикул</th><th>Название</th><th>Цена</th></tr></thead>');
    }
}